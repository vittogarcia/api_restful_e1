from django.contrib import admin
from django.urls import path, re_path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    ####### URL APPS INSTALLED
    # re_path('', include('applications.alumno.urls')),
    # re_path('', include('applications.maestro.urls')),
    # re_path('', include('applications.materia.urls')),
]
